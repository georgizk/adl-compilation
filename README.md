#Introduction

ADL is a language and framework for describing processor cores. The software is open source provided from freescale and can be found at http://opensource.freescale.com/fsl-oss-projects/

#Requirements

Several software packages are needed for the newest ADL version to compile. There could be more, but these are the ones I have identified:

- GCC 4.4
- TCL (not sure which version)
- Boost 1.53
- flex (not sure which version)
- rnumber
- plasma
- bison 2.4
- autoconf 2.64

#Building GCC

If you do not have root access to the system, you might need to compile GCC yourself. If running on a supercomputer node (Colosse, Guillimin), you can select the GCC module. Use `module avail` and `module load /name/of/gcc/module` to select the right one.

Before you begin, it's a good idea to read through [the FAQ](http://gcc.gnu.org/wiki/FAQ#configure "GCC Configuration FAQ")

Create the needed folders

    mkdir ~/gccsrc
    mkdir ~/gcc-build
    mkdir ~/gcc-4.4.7

Download, extract, and compile the prerequisites

    cd ~/gccsrc
    wget http://gcc.skazkaforyou.com/releases/gcc-4.4.7/gcc-4.4.7.tar.gz
    tar zxvf gcc-4.4.7.tar.gz

    cd ~/gccsrc
    wget ftp://gcc.gnu.org/pub/gcc/infrastructure/gmp-4.3.2.tar.bz2
    tar jxvf gmp-4.3.2.tar.bz2
    cd gmp-4.3.2
    ./configure --prefix=$HOME/gcc-4.4.7 
    make
    make install

    cd ~/gccsrc
    wget ftp://gcc.gnu.org/pub/gcc/infrastructure/mpfr-2.4.2.tar.bz2
    tar jxvf mpfr-2.4.2.tar.bz2
    cd mpfr-2.4.2
    ./configure --prefix=$HOME/gcc-4.4.7 --with-gmp=$HOME/gcc-4.4.7
    make
    make install

Modify your environment variables. I recommend editing ~/.bashrc and running `source ~/.bashrc` but you don't need to. Also add the adl bin to path while you're at it.

    export LD_LIBRARY_PATH=$HOME/gcc-4.4.7/lib:$HOME/gcc-4.4.7/lib64  
    export PATH=$HOME/gcc-4.4.7/bin:$HOME/adl/bin:$PATH
    export CPATH=$HOME/adl/include:$HOME/gcc-4.4.7/include:$CPATH

Compile GCC

    cd ~/gcc-build
    ~/gccsrc/gcc-4.4.7/configure --with-gmp=$HOME/gcc-4.4.7 --with-mpfr=$HOME/gcc-4.4.7 --prefix=$HOME/gcc-4.4.7 --enable-shared --enable-threads=posix --enable-checking=release --with-system-zlib --enable-__cxa_atexit --disable-libunwind-exceptions --enable-libgcj-multifile --enable-languages=c,c++,objc,obj-c++ --disable-dssi --disable-plugin --with-cpu=generic
    make
    make install


#Compiling ADL

The assumption is that you have gcc 4.4 at this point. The software you need can be obtained from the following locations: 

- [flex](http://www.opensource.apple.com/source/flex)
- [plasma](http://opensource.freescale.com/fsl-oss-projects/plasma/)
- [rnumber](http://opensource.freescale.com/fsl-oss-projects/rnumber/)
- [adl](http://opensource.freescale.com/fsl-oss-projects/adl/)
- [boost](http://www.boost.org/)
- [tcl](http://wiki.tcl.tk/20786)
- [bison](http://www.gnu.org/software/bison/)
- [autoconf](http://www.gnu.org/software/autoconf/)

Download all the necessary archives

    wget http://www.opensource.apple.com/source/flex/flex-26/flex-2.5.35.tar.bz2
    wget http://opensource.freescale.com/fsl-oss-projects/adl/adl-3.5.5.tar.bz2
    wget http://opensource.freescale.com/fsl-oss-projects/rnumber/rnumber-2.1.26.tar.bz2
    wget http://opensource.freescale.com/fsl-oss-projects/plasma/plasma-0.9.0.tar.bz2
    wget http://downloads.sourceforge.net/project/boost/boost/1.54.0/boost_1_54_0.tar.gz
    wget http://sourceforge.net/projects/tcl/files/latest/download
    wget http://ftp.gnu.org/gnu/bison/bison-2.4.tar.gz
    wget http://ftp.gnu.org/gnu/autoconf/autoconf-2.64.tar.gz

Extract them

    tar jxvf flex-2.5.35.tar.bz2
    tar jxvf plasma-0.9.0.tar.bz2
    tar jxvf rnumber-2.1.26.tar.bz2
    tar jxvf adl-3.5.5.tar.bz2
    tar zxvf boost_1_54_0.tar.gz
    tar zxvf tcl8.6.1-src.tar.gz
    tar zxvf bison-2.4.tar.gz
    tar zxvf autoconf-2.64.tar.gz

Create the folder where everything will be compiled

    mkdir ~/adl

Make sure the new bin is in your path. Additionally might need to add the gcc paths. You might have already done this step if you compiled gcc yourself.

    export LD_LIBRARY_PATH=$HOME/gcc-4.4.7/lib:$HOME/gcc-4.4.7/lib64  
    export PATH=$HOME/gcc-4.4.7/bin:$HOME/adl/bin:$PATH
    export CPATH=$HOME/adl/include:$HOME/gcc-4.4.7/include:$CPATH

Compile the prerequisites

    cd ~/tcl8.6.1/unix
    ./configure --prefix=$HOME/adl
    make
    make install
    # create a link, otherwise tclsh isn't found
    ln -s $HOME/adl/bin/tclsh8.6 $HOME/adl/bin/tclsh

    cd ~/bison-2.4
    ./configure --prefix=$HOME/adl
    make
    make install

    cd ~/autoconf-2.64
    ./configure --prefix=$HOME/adl
    make
    make install

    cd ~/flex-2.5.35
    ./configure --prefix=$HOME/adl
    make
    make install

    cd ~/rnumber-2.1.26
    ./configure --enable-opt --prefix=$HOME/adl
    make all check install

    cd ~/plasma-0.9.0
    ./configure --enable-opt --prefix=$HOME/adl --with-flex-include=$HOME/adl/include/ 
    # if the test fails, just run make install. It failed for me, but ADL still worked fine
    make all check install

    cd ~/boost_1_54_0
    ./bootstrap.sh --prefix=$HOME/adl
    ./b2 install

Build ADL

    cd ~/adl-3.5.5
    ./configure --enable-opt --prefix=$HOME/adl --with-plasma=$HOME/adl/bin/ --with-rnumber=$HOME/adl/bin  --with-flex-include=$HOME/adl/include/ --with-boost=$HOME/adl --with-tcl=$HOME/adl/bin --with-tclinc=$HOME/adl/include
    make all check install

#Building the model

The whole goal of the exercise was to have an up-to-date ADL binary for building the iss binary. We use it to run elf files and simulate fault injection.

You can build the model using

    ~/adl/bin/adl2iss --decode-cache ~/adl-3.5.5/designs/generic32.adl

The `--decode-cache` option should make simulation faster, though I haven't confirmed its effects.

#Setting up your environment

To run the simulator correctly, you need to have the required libraries in your path. This can be achieved by modifying your .bashrc as outlined in the compilation instructions.

On Colosse and Guillimin, you can also create your own module and define the paths there. A sample would be

    #%Module1.0#####################################################################
    ##
    ##
    ## $Id:adl,v 1.1 2013-09-29 21:47:06 dale Exp $
    ##

    proc ModulesHelp { } {
        puts stderr "\tSets up environment for running ADL  "
    }


    module-whatis   "Sets up environment for running ADL "

    ########################################################
    # ADL
    set             projectroot        /home/georgi
    set             adlroot            $projectroot/adl
    set             gccroot            $projectroot/gcc-4.4.7
    prepend-path    PATH               $gccroot/bin:$adlroot/bin
    prepend-path    LD_LIBRARY_PATH    $gccroot/lib:$gccroot/lib64
    prepend-path    CPATH              $gccroot/include

    setenv          FC                      gfortran
    setenv          F77                     gfortran
    setenv          CC                      gcc
    setenv          CXX                     g++

Then just load it using `module load /absolute/path/to/module`
