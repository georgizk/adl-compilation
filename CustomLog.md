#Compiling CustomLog Plugin

Compilation is simple - simply run 

	g++ `$adl/scripts/adl-config --cflags` CustomLog.C -shared -o custom-log.so -fPIC
	
where `$adl` holds the path of the ADL source directory. 

For details, please refer to adl-iss-plugins in the doc folder in the ADL source directory.

#Using CustomLog Plugin

All is as usual, with the addition of the --plugin , -o and -of command line arguments:

	./generic32 basicmath_small.elf -dc -fep -sce -mic=100000000 -o=log.bin -of=bin --plugin=./custom-log.so -trace --script=regular.cli
	
The regular.cli script needs to be modified to disable printing to stdout.

However, note that if the -dc option is used, the --script option has to be used, otherwise no log is generated. 
Additionally, if the --script option is used, you need to make sure to include a `quit` at the end, otherwise the last block does not get written.